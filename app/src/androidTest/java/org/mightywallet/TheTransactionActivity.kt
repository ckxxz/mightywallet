package org.mightywallet

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import org.junit.Rule
import org.junit.Test
import org.kethereum.model.Transaction
import org.ligi.trulesk.TruleskActivityRule
import org.mightywallet.activities.ViewTransactionActivity
import org.mightywallet.activities.ViewTransactionActivity.Companion.getTransactionActivityIntentForHash
import org.mightywallet.data.ETH_IN_WEI
import org.mightywallet.data.transactions.TransactionState
import org.mightywallet.data.transactions.TransactionWithState
import org.mightywallet.infrastructure.TestApp
import org.mightywallet.testdata.AddressBookWithTestEntries.Companion.Room77
import org.mightywallet.testdata.AddressBookWithTestEntries.Companion.ShapeShift
import org.mightywallet.testdata.DEFAULT_TEST_ADDRESS
import java.math.BigInteger

class TheTransactionActivity {

    @get:Rule
    var rule = TruleskActivityRule(ViewTransactionActivity::class.java, false)

    private val DEFAULT_NONCE = BigInteger("11")
    @Test
    fun nonceIsDisplayedCorrectly() {
        TestApp.transactionProvider.addTransaction(TransactionWithState(Transaction(ETH_IN_WEI, DEFAULT_TEST_ADDRESS, DEFAULT_TEST_ADDRESS, nonce = DEFAULT_NONCE, txHash = "0xFOO"), TransactionState()))

        rule.launchActivity(InstrumentationRegistry.getContext().getTransactionActivityIntentForHash("0xFOO"))

        onView(withId(R.id.nonce)).check(matches(withText("11")))
    }

    @Test
    fun isLabeledToWhenWeReceive() {
        val transaction = Transaction(ETH_IN_WEI, from = DEFAULT_TEST_ADDRESS, to = Room77, nonce = DEFAULT_NONCE, txHash = "0xFOO12")
        TestApp.transactionProvider.addTransaction(TransactionWithState(transaction, TransactionState()))

        rule.launchActivity(InstrumentationRegistry.getContext().getTransactionActivityIntentForHash(transaction.txHash!!))

        onView(withId(R.id.from_to_title)).check(matches(withText(R.string.transaction_to_label)))
        onView(withId(R.id.from_to)).check(matches(withText("Room77")))
    }


    @Test
    fun isLabeledFromWhenWeReceive() {
        val transaction = Transaction(ETH_IN_WEI, from = ShapeShift, to = DEFAULT_TEST_ADDRESS, nonce = DEFAULT_NONCE, txHash = "0xFOO21")
        TestApp.transactionProvider.addTransaction(TransactionWithState(transaction, TransactionState()))

        rule.launchActivity(InstrumentationRegistry.getContext().getTransactionActivityIntentForHash(transaction.txHash!!))

        onView(withId(R.id.from_to_title)).check(matches(withText(R.string.transaction_from_label)))
        onView(withId(R.id.from_to)).check(matches(withText("ShapeShift")))
    }


}
