package org.mightywallet.testdata

import org.mightywallet.data.BalanceProvider

class BalanceProviderWithResetFun : BalanceProvider() {

    fun reset() {
        balanceMap.clear()
    }
}
