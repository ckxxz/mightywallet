package org.mightywallet.infrastructure

import android.support.v7.app.AppCompatDelegate.MODE_NIGHT_YES
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mightywallet.App
import org.mightywallet.data.BalanceProvider
import org.mightywallet.data.addressbook.AddressBook
import org.mightywallet.data.config.Settings
import org.mightywallet.data.exchangerate.ExchangeRateProvider
import org.mightywallet.data.exchangerate.TokenProvider
import org.mightywallet.data.keystore.WallethKeyStore
import org.mightywallet.data.syncprogress.SyncProgressProvider
import org.mightywallet.data.syncprogress.WallethSyncProgress
import org.mightywallet.data.tokens.FileBackedTokenProvider
import org.mightywallet.data.transactions.TransactionProvider
import org.mightywallet.testdata.*

class TestApp : App() {

    override fun createKodein() = Kodein.Module {
        bind<AddressBook>() with singleton { addressBookWithEntries }
        bind<BalanceProvider>() with singleton { balanceProvider }
        bind<TransactionProvider>() with singleton { transactionProvider }
        bind<ExchangeRateProvider>() with singleton { fixedValueExchangeProvider }
        bind<TokenProvider>() with singleton { FileBackedTokenProvider(this@TestApp,instance()) }
        bind<SyncProgressProvider>() with singleton {
            SyncProgressProvider().apply {
                setSyncProgress(WallethSyncProgress(true, 42000, 42042))
            }
        }
        bind<WallethKeyStore>() with singleton { keyStore }
        bind<Settings>() with singleton {

            mock(Settings::class.java).apply {
                `when`(currentFiat).thenReturn("EUR")
                `when`(getNightMode()).thenReturn(MODE_NIGHT_YES)
                `when`(startupWarningDone).thenReturn(true)
            }
        }
    }

    override fun executeCodeWeWillIgnoreInTests() = Unit

    companion object {
        val transactionProvider = TransactionProviderWithTestData()
        val fixedValueExchangeProvider = FixedValueExchangeProvider()
        val balanceProvider = BalanceProviderWithResetFun()
        val addressBookWithEntries = AddressBookWithTestEntries()
        val keyStore = TestKeyStore()
    }
}
