package org.mightywallet.ui


import android.content.*
import android.os.Bundle
import android.support.v7.preference.CheckBoxPreference
import android.support.v7.preference.PreferenceFragmentCompat
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import org.ligi.kaxt.recreateWhenPossible
import org.mightywallet.App
import org.mightywallet.R
import org.mightywallet.core.GethLightEthereumService
import org.mightywallet.core.GethLightEthereumService.Companion.gethStopIntent
import org.mightywallet.data.config.Settings
import org.mightywallet.data.exchangerate.TokenProvider
import android.support.v7.preference.Preference
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_show_qr.*
import org.mightywallet.data.keystore.WallethKeyStore
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.policy.actions.DynamoDBv2Actions.Query;
import com.amazonaws.models.nosql.UserBackUpDO
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobile.auth.core.IdentityManager
import com.mtramin.rxfingerprint.EncryptionMethod
import com.mtramin.rxfingerprint.RxFingerprint
import com.mtramin.rxfingerprint.data.FingerprintResult
import org.mightywallet.data.DEFAULT_PASSWORD
import android.content.Context.CLIPBOARD_SERVICE




class WalletPrefsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {


    val keyStore: WallethKeyStore by LazyKodein(appKodein).instance()
    val settings: Settings by LazyKodein(appKodein).instance()
    val tokenProvider: TokenProvider by LazyKodein(appKodein).instance()
    private val DEFAULT_KEY_NAME = "default_key"

    override fun onResume() {
        super.onResume()


        //var dynamoDBMapper: DynamoDBMapper! = null
        var userId = ""

        val awsConfig = AWSConfiguration(context)
        val credentialsProvider = IdentityManager.getDefaultIdentityManager().credentialsProvider //AWSIdentityManager.getDefault().getCredentialsProvider()
        val identityManager = IdentityManager(context, awsConfig)
        userId = identityManager.getCachedUserID()
        val dynamoDBClient = AmazonDynamoDBClient(credentialsProvider)
        var dynamoDBMapper: DynamoDBMapper = DynamoDBMapper.builder()
                .dynamoDBClient(dynamoDBClient)
                .awsConfiguration(awsConfig)
                .build()

        var backupItem:UserBackUpDO? = null

        Thread(Runnable {
            backupItem = dynamoDBMapper.load(UserBackUpDO::class.java, userId)

            // Item saved
        }).start()

        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
        findPreference(getString(R.string.key_reference)).summary = "Currently: " + settings.currentFiat
        findPreference(getString(R.string.key_token)).summary = "Currently: " + tokenProvider.currentToken.name

        val button = findPreference(getString(R.string.key_fingerprint))
        button.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            val fragment = FingerprintAuthenticationDialogFragment()
            fragment.show(activity.fragmentManager, "tag1")
            true
        }

        val button2 = findPreference(getString(R.string.key_fingerprint_restore))
        button2.onPreferenceClickListener = Preference.OnPreferenceClickListener {

            RxFingerprint.decrypt(EncryptionMethod.RSA, context, DEFAULT_KEY_NAME, backupItem!!.backupId1)
                    .subscribe({ decryptionResult ->
                        when (decryptionResult.result) {
                            FingerprintResult.FAILED -> Toast.makeText(context,  "Fingerprint not recognized, try again!"  , Toast.LENGTH_LONG).show()
                            FingerprintResult.HELP ->  Toast.makeText(context,  decryptionResult.message  , Toast.LENGTH_LONG).show()
                            FingerprintResult.AUTHENTICATED -> {

                                Toast.makeText(context, "decrypted:\n" + decryptionResult.decrypted + backupItem!!.backupId2, Toast.LENGTH_LONG).show()
                                setClipBoardLink(context,decryptionResult.decrypted + backupItem!!.backupId2)
                                }
                            }
                    }) { throwable ->

                        if (RxFingerprint.keyInvalidated(throwable)) {
                            // The keys you wanted to use are invalidated because the user has turned off his
                            // secure lock screen or changed the fingerprints stored on the device
                            // You have to re-encrypt the data to access it
                        }
                        Log.e("ERROR", "decrypt", throwable)
                        Toast.makeText(context, throwable.message  , Toast.LENGTH_LONG).show()
                    }
            true
        }

        //셋팅 쪽 안보이게 처리
        //setUserNameSummary()
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key == getString(R.string.key_prefs_day_night)) {

            App.applyNightMode(settings)
            activity.recreateWhenPossible()
        }
        if (key == getString(R.string.key_prefs_start_light)) {
            if ((findPreference(key) as CheckBoxPreference).isChecked != GethLightEthereumService.isRunning) {
                if (GethLightEthereumService.isRunning) {
                    context.startService(context.gethStopIntent())
                } else {
                    context.startService(Intent(context, GethLightEthereumService::class.java))
                }
            }

        }

        //setUserNameSummary()

    }

    fun setClipBoardLink(context: Context, link: String) {

        val clipboardManager = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("label", link)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(context, "복사되었습니다.", Toast.LENGTH_SHORT).show()

    }

    private fun setUserNameSummary() {
        findPreference(getString(R.string.key_prefs_stats_username)).summary = settings.getStatsName() + " @ https://stats.rinkeby.io"
    }

    override fun onCreatePreferences(bundle: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }



}