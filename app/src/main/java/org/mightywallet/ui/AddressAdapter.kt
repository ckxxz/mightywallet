package org.mightywallet.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import org.mightywallet.R
import org.mightywallet.data.addressbook.AddressBookEntry
import org.mightywallet.data.keystore.WallethKeyStore

class AddressAdapter(val list: List<AddressBookEntry>,
                     val keyStore: WallethKeyStore,
                     val onClickAction: (entry: AddressBookEntry) -> Unit,
                     val onLongClickAction: (entry: AddressBookEntry) -> Unit) : RecyclerView.Adapter<AddressViewHolder>() {

    override fun getItemCount() = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val inflate = LayoutInflater.from(parent.context).inflate(R.layout.item_address_book, parent, false)
        return AddressViewHolder(inflate)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(list[position], keyStore, onClickAction, onLongClickAction)
    }

}