package org.mightywallet.ui

import android.content.Context
import android.util.AttributeSet
import org.mightywallet.R

class ValueViewSmall(context: Context, attrs: AttributeSet) : ValueView(context, attrs) {

    override val layoutRes = R.layout.value_small

}
