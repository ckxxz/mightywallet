package org.mightywallet.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.value.view.*
import org.ligi.kaxt.setVisibility
import org.mightywallet.R
import org.mightywallet.data.config.Settings
import org.mightywallet.data.exchangerate.ETH_TOKEN
import org.mightywallet.data.exchangerate.ExchangeRateProvider
import org.mightywallet.data.tokens.TokenDescriptor
import org.mightywallet.functions.toValueString
import java.math.BigInteger

open class ValueView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    val exchangeRateProvider: ExchangeRateProvider by LazyKodein(appKodein).instance()
    val settings: Settings by LazyKodein(appKodein).instance()

    open val layoutRes = R.layout.value

    override fun onFinishInflate() {
        super.onFinishInflate()
        orientation = VERTICAL
        LayoutInflater.from(context).inflate(layoutRes, this, true)
    }

    fun setValue(int: BigInteger, token: TokenDescriptor) {

        val exChangeRate = exchangeRateProvider.getExchangeString(int, settings.currentFiat)

        if (token == ETH_TOKEN) {
            current_fiat_symbol.text = settings.currentFiat
            if (exChangeRate != null) {
                current_fiat.text = exChangeRate
            } else {
                current_fiat.text = "?"
            }
        }

        current_token_symbol.text = token.name

        current_fiat_symbol.setVisibility(token == ETH_TOKEN)
        current_fiat.setVisibility(token == ETH_TOKEN)

        current_eth.text = int.toValueString(token)
    }

}
