package org.mightywallet.ui

interface ChangeObserver {
    fun observeChange()
}