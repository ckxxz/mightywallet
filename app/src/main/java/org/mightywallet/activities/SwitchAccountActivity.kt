package org.mightywallet.activities

import android.os.Bundle
import org.mightywallet.R
import org.mightywallet.data.DEFAULT_PASSWORD
import org.mightywallet.ui.AddressAdapter

class SwitchAccountActivity : BaseAddressBookActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.subtitle = getString(R.string.switch_account_subtitle)
    }

    override fun getAdapter() = AddressAdapter(addressBook.getAllEntries(), keyStore, onClickAction = {
        keyStore.setCurrentAddress(it.address)
        finish()
    }, onLongClickAction = {
        keyStore.deleteKey(it.address, DEFAULT_PASSWORD)
        addressBook.removeAddress(it.address)
        finish()
    })


}
