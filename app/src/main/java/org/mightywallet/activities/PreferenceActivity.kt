package org.mightywallet.activities


import android.annotation.TargetApi
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.preference.Preference
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.amazonaws.mobile.auth.core.IdentityManager
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper
import com.amazonaws.models.nosql.UserBackUpDO
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.mtramin.rxfingerprint.EncryptionMethod
import com.mtramin.rxfingerprint.RxFingerprint
import com.mtramin.rxfingerprint.data.FingerprintResult
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.operators.flowable.FlowableBlockingSubscribe.subscribe
import org.ethereum.geth.Geth
import org.mightywallet.R
import org.mightywallet.data.DEFAULT_GAS_LIMIT_ERC_20_TX
import org.mightywallet.data.DEFAULT_PASSWORD
import org.mightywallet.data.keystore.WallethKeyStore
import java.io.File
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.KeyGenerator


class PreferenceActivity : AppCompatActivity() {

    private val SECRET_MESSAGE = "Very secret message"
    private val DEFAULT_KEY_NAME = "default_key"
    private var mKeyStore: KeyStore? = null
    private var mKeyGenerator: KeyGenerator? = null

    var dynamoDBMapper: DynamoDBMapper? = null
    var userId = ""
    var stren1 = ""
    var stren2 = ""

    val keyStore: WallethKeyStore by LazyKodein(appKodein).instance()
    //private val keyStoreFile by lazy { File(context.filesDir, "keystore") }
    //val keyStore by lazy { org.ethereum.geth.KeyStore(keyStoreFile.absolutePath, Geth.LightScryptN, Geth.LightScryptP) }


    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_prefs)

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        try {
            try {
                mKeyStore = KeyStore.getInstance("AndroidKeyStore")
            } catch (e: KeyStoreException) {
                throw RuntimeException("Failed to get an instance of KeyStore", e)
            }

            try {
                mKeyGenerator = KeyGenerator
                        .getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
            } catch (e: NoSuchAlgorithmException) {
                throw RuntimeException("Failed to get an instance of KeyGenerator", e)
            } catch (e: NoSuchProviderException) {
                throw RuntimeException("Failed to get an instance of KeyGenerator", e)
            }
        }catch (e: Exception)
        {
            e.printStackTrace();
        }

        val awsConfig = AWSConfiguration(applicationContext)
        val credentialsProvider = IdentityManager.getDefaultIdentityManager().credentialsProvider //AWSIdentityManager.getDefault().getCredentialsProvider()
        val identityManager = IdentityManager(applicationContext, awsConfig)
        userId = identityManager.getCachedUserID()
        val dynamoDBClient = AmazonDynamoDBClient(credentialsProvider)
        this.dynamoDBMapper = DynamoDBMapper.builder()
                .dynamoDBClient(dynamoDBClient)
                .awsConfiguration(awsConfig)
                .build()


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @TargetApi(23)
    fun createKey(keyName: String, invalidatedByBiometricEnrollment: Boolean) {
        try {
            mKeyStore?.load(null)

            val builder = KeyGenParameterSpec.Builder(keyName,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment)
            }
            mKeyGenerator?.init(builder.build())
            mKeyGenerator?.generateKey()
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        } catch (e: InvalidAlgorithmParameterException) {
            throw RuntimeException(e)
        } catch (e: CertificateException) {
            throw RuntimeException(e)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

    }

    fun onPurchased(withFingerprint: Boolean,
                    cryptoObject: FingerprintManager.CryptoObject?,
                    strtag : String) {
        if (withFingerprint) {
            assert(cryptoObject != null)

            //Toast.makeText(applicationContext,  keyStore.exportCurrentKey(DEFAULT_PASSWORD, "").  ,Toast.LENGTH_LONG).show()

            if(strtag.equals("tag1")) {

                var str = keyStore.exportCurrentKey(DEFAULT_PASSWORD, "");

                setClipBoardLink(this, str);

                RxFingerprint.encrypt(EncryptionMethod.RSA, this, DEFAULT_KEY_NAME, str.substring(0,200))
                        .subscribe({ encryptionResult ->
                            when (encryptionResult.result) {
                                FingerprintResult.FAILED -> Toast.makeText(applicationContext,  "Fingerprint not recognized, try again!"  ,Toast.LENGTH_LONG).show()
                                FingerprintResult.HELP -> Toast.makeText(applicationContext,  encryptionResult.message  ,Toast.LENGTH_LONG).show()
                                FingerprintResult.AUTHENTICATED -> {
                                    Toast.makeText(applicationContext, "Successfully authenticated! : " + encryptionResult.encrypted, Toast.LENGTH_LONG).show()
                                    stren1 = encryptionResult.encrypted
                                    stren2 = str.substring(200,str.length)
                                    createBackUpItem(stren1, stren2);
                                    }
                                }
                        }) { throwable ->
                            Log.e("ERROR", "authenticate", throwable)
                            Toast.makeText(applicationContext, throwable.message  ,Toast.LENGTH_LONG).show()
                        }



            }


            //tryEncrypt(cryptoObject!!.cipher)

            //Toast.makeText(MainActivity.this, "Cipher : " + cryptoObject.getCipher() , Toast.LENGTH_SHORT).show();
        } else {
            //showConfirmation(null, null)
        }
    }

    private fun tryEncrypt(cipher: Cipher) {
        try {
            val encrypted = cipher.doFinal(keyStore.exportCurrentKey(DEFAULT_PASSWORD, "").toByteArray())
            //createBackUpItem(encrypted)
            //showConfirmation(encrypted, cipher)


        } catch (e: BadPaddingException) {
            Toast.makeText(this, "Failed to encrypt the data with the generated key. " + "Retry the purchase", Toast.LENGTH_LONG).show()
            Log.e("SelectReferenceActivity", "Failed to encrypt the data with the generated key." + e.message)
        } catch (e: IllegalBlockSizeException) {
            Toast.makeText(this, "Failed to encrypt the data with the generated key. " + "Retry the purchase", Toast.LENGTH_LONG).show()
            Log.e("SelectReferenceActivity", "Failed to encrypt the data with the generated key." + e.message)
        }

    }

    fun createBackUpItem(text1: String, text2: String) {
        val backupItem = UserBackUpDO()

        // Use IdentityManager to get the user identity id.
        backupItem.setUserId(this.userId)
        backupItem.setBackupId1(text1)
        backupItem.setBackupId2(text2)

        Thread(Runnable {
            dynamoDBMapper?.save(backupItem)

            // Item saved
        }).start()
    }


    fun setClipBoardLink(context: Context, link: String) {

        val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("label", link)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(context, "복사되었습니다.", Toast.LENGTH_SHORT).show()

    }

}