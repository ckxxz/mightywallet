package org.mightywallet.activities

import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View.INVISIBLE
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_in_drawer_container.*
import kotlinx.android.synthetic.main.value.*
import org.json.JSONObject
import org.kethereum.functions.isERC67String
import org.ligi.kaxt.recreateWhenPossible
import org.ligi.kaxt.setVisibility
import org.ligi.kaxt.startActivityFromClass
import org.ligi.kaxtui.alert
import org.ligi.tracedroid.TraceDroid
import org.ligi.tracedroid.sending.TraceDroidEmailSender
import org.mightywallet.R
import org.mightywallet.activities.qrscan.startScanActivityForResult
import org.mightywallet.data.BalanceAtBlock
import org.mightywallet.data.BalanceProvider
import org.mightywallet.data.addressbook.AddressBook
import org.mightywallet.data.config.Settings
import org.mightywallet.data.exchangerate.TokenProvider
import org.mightywallet.data.keystore.WallethKeyStore
import org.mightywallet.data.syncprogress.SyncProgressProvider
import org.mightywallet.data.transactions.TransactionProvider
import org.mightywallet.ui.ChangeObserver
import org.mightywallet.ui.TransactionAdapterDirection.INCOMMING
import org.mightywallet.ui.TransactionAdapterDirection.OUTGOING
import org.mightywallet.ui.TransactionRecyclerAdapter
import java.math.BigInteger
//import sun.security.krb5.KrbException.errorMessage
import android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23.authenticate
import android.widget.Toast
import com.amazonaws.mobile.auth.core.StartupAuthResult
import com.amazonaws.mobile.auth.core.StartupAuthResultHandler
import com.amazonaws.mobile.auth.core.IdentityManager
import com.amazonaws.mobile.config.AWSConfiguration






class MainActivity : AppCompatActivity() {

    val lazyKodein = LazyKodein(appKodein)

    val actionBarDrawerToggle by lazy { ActionBarDrawerToggle(this, drawer_layout, R.string.drawer_open, R.string.drawer_close) }

    val balanceProvider: BalanceProvider by lazyKodein.instance()
    val transactionProvider: TransactionProvider by lazyKodein.instance()
    val tokenProvider: TokenProvider by lazyKodein.instance()
    val syncProgressProvider: SyncProgressProvider by lazyKodein.instance()
    val addressBook: AddressBook by lazyKodein.instance()
    val keyStore: WallethKeyStore by lazyKodein.instance()
    val settings: Settings by lazyKodein.instance()
    var lastNightMode: Int? = null

    override fun onResume() {
        super.onResume()

        if (lastNightMode != null && lastNightMode != settings.getNightMode()) {
            recreateWhenPossible()
            return
        }
        lastNightMode = settings.getNightMode()

        syncProgressProvider.registerChangeObserverWithInitialObservation(object : ChangeObserver {
            override fun observeChange() {
                runOnUiThread {
                    val progress = syncProgressProvider.currentSyncProgress

                    if (progress.isSyncing) {
                        val percent = ((progress.currentBlock.toDouble() / progress.highestBlock) * 100).toInt()
                        supportActionBar?.subtitle = "Block ${progress.currentBlock}/${progress.highestBlock} ($percent%)"
                    }
                }
            }
        })

        transactionProvider.registerChangeObserverWithInitialObservation(object : ChangeObserver {
            override fun observeChange() {
                val allTransactions = transactionProvider.getTransactionsForAddress(keyStore.getCurrentAddress())
                val incomingTransactions = allTransactions.filter { it.transaction.to == keyStore.getCurrentAddress() }.sortedByDescending { it.transaction.creationEpochSecond }
                val outgoingTransactions = allTransactions.filter { it.transaction.from == keyStore.getCurrentAddress() }.sortedByDescending { it.transaction.creationEpochSecond }

                val hasNoTransactions = incomingTransactions.size + outgoingTransactions.size == 0

                runOnUiThread {
                    transaction_recycler_out.adapter = TransactionRecyclerAdapter(outgoingTransactions, addressBook, tokenProvider, OUTGOING)
                    transaction_recycler_in.adapter = TransactionRecyclerAdapter(incomingTransactions, addressBook, tokenProvider, INCOMMING)

                    empty_view_container.setVisibility(hasNoTransactions)

                    send_container.setVisibility(!hasNoTransactions, INVISIBLE)

                    transaction_recycler_in.setVisibility(!hasNoTransactions)
                    transaction_recycler_out.setVisibility(!hasNoTransactions)

                }
            }
        })
        balanceProvider.registerChangeObserverWithInitialObservation(object : ChangeObserver {
            override fun observeChange() {
                var balanceForAddress = BalanceAtBlock(balance = BigInteger("0"), block = 0, tokenDescriptor = tokenProvider.currentToken)
                balanceProvider.getBalanceForAddress(keyStore.getCurrentAddress(), tokenProvider.currentToken)?.let {
                    balanceForAddress = it
                }

                runOnUiThread {
                    value_view.setValue(balanceForAddress.balance, tokenProvider.currentToken)

                    if (!syncProgressProvider.currentSyncProgress.isSyncing) {
                        supportActionBar?.subtitle = "Block " + balanceForAddress.block
                    }
                }
            }
        })


    }

    fun String.isJSONKey() = try {
        JSONObject(this).let {
            it.has("address") && (it.has("crypto") || it.has("Crypto"))
        }
    } catch (e: Exception) {
        false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null && data.hasExtra("SCAN_RESULT")) {
            val scanResult = data.getStringExtra("SCAN_RESULT")

            when {
                scanResult.isERC67String() -> {
                    startActivity(Intent(this, CreateTransactionActivity::class.java).apply {
                        setData(Uri.parse(scanResult))
                    })
                }

                scanResult.length == 64 -> {
                    startActivity(getKeyImportIntent(scanResult, KeyType.ECDSA))
                }

                scanResult.isJSONKey() -> {
                    startActivity(getKeyImportIntent(scanResult, KeyType.JSON))
                }

                scanResult.startsWith("0x") -> {
                    AlertDialog.Builder(this)
                            .setTitle(R.string.select_action_messagebox_title)
                            .setItems(R.array.scan_hex_choices, { _, which ->
                                when (which) {
                                    0 -> {
                                        startCreateAccountActivity(scanResult)
                                    }
                                    1 -> {
                                        startActivity(Intent(this, CreateTransactionActivity::class.java).apply {
                                            setData(Uri.parse("ethereum:$scanResult"))
                                        })
                                    }
                                    2 -> {
                                        alert("TODO")
                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, null)
                            .show()
                }

                else -> {
                    AlertDialog.Builder(this)
                            .setMessage(R.string.scan_not_interpreted_error_message)
                            .setPositiveButton(android.R.string.ok, null)
                            .show()
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!settings.startupWarningDone) {
            alert(title = "알림", message = "이 앱은 베타 버전으로 test-network를 사용합니다. 실제 돈이 아님에 유의하세요!")
            settings.startupWarningDone = true
        }

        if (TraceDroid.getStackTraceFiles().isNotEmpty()) {
            TraceDroidEmailSender.sendStackTraces("ligi@ligi.de", this)
        }

        val appContext = applicationContext
        val awsConfig = AWSConfiguration(appContext)
        val identityManager = IdentityManager(appContext, awsConfig)
        IdentityManager.setDefaultIdentityManager(identityManager)
        identityManager.doStartupAuth(this) {
            // User identity is ready as unauthenticated user or previously signed-in user.
        }

        setContentView(R.layout.activity_main_in_drawer_container)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        drawer_layout.addDrawerListener(actionBarDrawerToggle)

        receive_container.setOnClickListener {
            startActivityFromClass(RequestActivity::class)
        }

        send_container.setOnClickListener {
            startActivityFromClass(CreateTransactionActivity::class)
        }

        fab.setOnClickListener {
            startScanActivityForResult(this)
        }

        transaction_recycler_out.layoutManager = LinearLayoutManager(this)
        transaction_recycler_in.layoutManager = LinearLayoutManager(this)

        current_fiat_symbol.setOnClickListener {
            startActivityFromClass(SelectReferenceActivity::class)
        }

        current_token_symbol.setOnClickListener {
            startActivityFromClass(SelectTokenActivity::class)
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        actionBarDrawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        actionBarDrawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_info -> {
            startActivityFromClass(InfoActivity::class.java)
            true
        }
        else -> actionBarDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item)
    }

}
