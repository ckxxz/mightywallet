package org.mightywallet.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import org.mightywallet.R
import org.mightywallet.ui.AddressAdapter

open class AddressBookActivity : BaseAddressBookActivity() {

    override fun getAdapter() = AddressAdapter(addressBook.getAllEntries(), keyStore,
            onClickAction = {
        setResult(Activity.RESULT_OK, Intent().apply { putExtra("HEX", it.address.hex) })
        finish()} ,
            onLongClickAction = {
        setResult(Activity.RESULT_OK, Intent().apply { putExtra("HEX", it.address.hex) })
        finish()} )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.subtitle = getString(R.string.address_book_subtitle)
    }

}
