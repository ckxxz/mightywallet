package org.mightywallet

import android.content.Context
import android.content.Intent
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.amazonaws.mobile.auth.core.IdentityManager
import com.chibatching.kotpref.Kotpref
import com.github.salomonbrys.kodein.*
import com.jakewharton.threetenabp.AndroidThreeTen
import okhttp3.OkHttpClient
import org.ligi.tracedroid.TraceDroid
import org.mightywallet.core.EtherScanService
import org.mightywallet.core.GethLightEthereumService
import org.mightywallet.core.GethTransactionSigner
import org.mightywallet.core.TransactionNotificationService
import org.mightywallet.data.BalanceProvider
import org.mightywallet.data.addressbook.AddressBook
import org.mightywallet.data.addressbook.FileBackedAddressBook
import org.mightywallet.data.config.KotprefSettings
import org.mightywallet.data.config.Settings
import org.mightywallet.data.exchangerate.CryptoCompareExchangeProvider
import org.mightywallet.data.exchangerate.ExchangeRateProvider
import org.mightywallet.data.exchangerate.TokenProvider
import org.mightywallet.data.keystore.GethBackedWallethKeyStore
import org.mightywallet.data.keystore.WallethKeyStore
import org.mightywallet.data.networks.NetworkDefinitionProvider
import org.mightywallet.data.syncprogress.SyncProgressProvider
import org.mightywallet.data.tokens.FileBackedTokenProvider
import org.mightywallet.data.transactions.BaseTransactionProvider
import org.mightywallet.data.transactions.TransactionProvider
import com.amazonaws.mobile.config.AWSConfiguration





open class App : MultiDexApplication(), KodeinAware {

    override val kodein by Kodein.lazy {
        bind<OkHttpClient>() with singleton { OkHttpClient.Builder().build() }
        bind<NetworkDefinitionProvider>() with singleton { NetworkDefinitionProvider() }

        import(createKodein())
    }

    open fun createKodein() = Kodein.Module {
        bind<AddressBook>() with singleton { FileBackedAddressBook(this@App) }
        bind<BalanceProvider>() with singleton { BalanceProvider() }
        bind<TransactionProvider>() with singleton { BaseTransactionProvider() }
        bind<ExchangeRateProvider>() with singleton { CryptoCompareExchangeProvider(this@App, instance()) }
        bind<SyncProgressProvider>() with singleton { SyncProgressProvider() }
        bind<WallethKeyStore>() with singleton { GethBackedWallethKeyStore(this@App) }
        bind<Settings>() with singleton { KotprefSettings }
        bind<TokenProvider>() with singleton { FileBackedTokenProvider(this@App, instance()) }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        Kotpref.init(this)
        TraceDroid.init(this)
        AndroidThreeTen.init(this)

        applyNightMode(kodein.instance())
        executeCodeWeWillIgnoreInTests()
        initializeApplication();
    }

    private fun initializeApplication() {

        val awsConfiguration = AWSConfiguration(applicationContext)

        // If IdentityManager is not created, create it
        if (IdentityManager.getDefaultIdentityManager() == null) {
            val identityManager = IdentityManager(applicationContext, awsConfiguration)
            IdentityManager.setDefaultIdentityManager(identityManager)
        }

    }
    open fun executeCodeWeWillIgnoreInTests() {
        if (KotprefSettings.isLightClientWanted()) {
            startService(Intent(this, GethLightEthereumService::class.java))
        }
        startService(Intent(this, GethTransactionSigner::class.java))
        startService(Intent(this, EtherScanService::class.java))
        startService(Intent(this, TransactionNotificationService::class.java))
    }

    companion object {
        fun applyNightMode(settings: Settings) {
            @AppCompatDelegate.NightMode val nightMode = settings.getNightMode()
            AppCompatDelegate.setDefaultNightMode(nightMode)
        }
    }
}

