package org.mightywallet.data.addressbook

import org.kethereum.model.Address
import org.mightywallet.data.Observeable

interface AddressBook : Observeable {

    fun getEntryForName(address: Address): AddressBookEntry?

    fun getAllEntries(): List<AddressBookEntry>

    fun setEntry(entry: AddressBookEntry)

    fun removeAddress(address: Address)

}