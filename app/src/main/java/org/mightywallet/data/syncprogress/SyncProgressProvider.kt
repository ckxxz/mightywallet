package org.mightywallet.data.syncprogress

import org.mightywallet.data.SimpleObserveable

class SyncProgressProvider : SimpleObserveable() {

    var currentSyncProgress = WallethSyncProgress(false, 0L, 0L)

    fun setSyncProgress(syncProgress: WallethSyncProgress) {
        if (currentSyncProgress != syncProgress) {
            currentSyncProgress = syncProgress
            promoteChange()
        }
    }
}