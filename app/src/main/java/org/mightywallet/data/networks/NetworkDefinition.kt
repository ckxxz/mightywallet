package org.mightywallet.data.networks

import org.mightywallet.data.blockexplorer.BlockExplorer

interface NetworkDefinition {
    fun getNetworkName(): String
    fun getBlockExplorer(): BlockExplorer
    val chainId: Long
    val genesis: String
    val bootNodes: List<String>
}