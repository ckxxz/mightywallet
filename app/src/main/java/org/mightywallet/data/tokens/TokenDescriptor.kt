package org.mightywallet.data.tokens

data class TokenDescriptor(val name: String, val decimals: Int, val address: String)