package org.mightywallet.data.transactions

enum class TransactionSource {
    WALLETH,
    WALLETH_PROCESSED,
    GETH,
    ETHERSCAN
}