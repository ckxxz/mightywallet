package org.mightywallet.data.blockexplorer

class EtherscanRopstenBlockExplorer() : EtherscanBlockExplorer() {

    override val base = "https://ropsten.etherscan.io/"

}