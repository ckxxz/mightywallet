package org.mightywallet.data.blockexplorer

class EtherscanRinkebyBlockExplorer() : EtherscanBlockExplorer() {

    override val base = "https://rinkeby.etherscan.io/"

}