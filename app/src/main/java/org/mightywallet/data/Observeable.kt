package org.mightywallet.data

import org.mightywallet.ui.ChangeObserver

interface Observeable {

    fun registerChangeObserver(changeObserver: ChangeObserver): Unit

    fun registerChangeObserverWithInitialObservation(changeObserver: ChangeObserver)

    fun unRegisterChangeObserver(changeObserver: ChangeObserver)

}