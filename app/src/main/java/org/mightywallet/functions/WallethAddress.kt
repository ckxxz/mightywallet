package org.mightywallet.functions

import org.kethereum.model.Address
import org.mightywallet.data.addressbook.AddressBook

fun Address.resolveNameFromAddressBook(addressBook: AddressBook)
        = addressBook.getEntryForName(this)?.name ?: hex
